package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.dto.request.ServerAboutRequest;
import ru.mtumanov.tm.dto.request.ServerVersionRequest;
import ru.mtumanov.tm.dto.response.ServerAboutResponse;
import ru.mtumanov.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {
    
    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest serverAboutRequest);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest serverVersionRequest);
    
}
