package ru.mtumanov.tm.component;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import ru.mtumanov.tm.api.endpoint.Operation;
import ru.mtumanov.tm.api.service.ILoggerService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.dto.request.AbstractRequest;
import ru.mtumanov.tm.dto.response.AbstractResponse;
import ru.mtumanov.tm.task.AbstractServerTask;
import ru.mtumanov.tm.task.ServerAcceptTask;

public final class Server {

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    @NotNull
    private final Bootstrap bootstrap;

    @Getter
    @NotNull
    private final ILoggerService loggerService;

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.loggerService = bootstrap.getLoggerService();
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.execute(task);
    }
    
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        try {
            serverSocket = new ServerSocket(port);
        } catch (@NotNull IOException e) {
            bootstrap.getLoggerService().error(e);
        }
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        return dispatcher.call(request);
    }

    public void stop() {
        if (serverSocket == null) return;
        executorService.shutdown();
        try {
            serverSocket.close();
        } catch (@NotNull IOException e) {
            bootstrap.getLoggerService().error(e);
        }
    }
}
