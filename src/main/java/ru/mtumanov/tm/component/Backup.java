package ru.mtumanov.tm.component;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.command.data.AbstractDataCommand;
import ru.mtumanov.tm.command.data.DataBase64LoadCommand;
import ru.mtumanov.tm.command.data.DataBase64SaveCommand;
import ru.mtumanov.tm.exception.AbstractException;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void save() {
        try {
            bootstrap.processCommand(DataBase64SaveCommand.NAME, false);
        } catch (AbstractException e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64)))
            return;
        try {
            bootstrap.processCommand(DataBase64LoadCommand.NAME, false);
        } catch (AbstractException e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void stop() {
        executorService.shutdown();
    }

}
