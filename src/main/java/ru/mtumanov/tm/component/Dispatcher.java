package ru.mtumanov.tm.component;

import java.util.LinkedHashMap;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.mtumanov.tm.api.endpoint.Operation;
import ru.mtumanov.tm.dto.request.AbstractRequest;
import ru.mtumanov.tm.dto.response.AbstractResponse;

public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();
    
    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull Class<RQ> reqClass, 
            @NotNull Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
