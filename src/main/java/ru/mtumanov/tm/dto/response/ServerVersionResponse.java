package ru.mtumanov.tm.dto.response;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServerVersionResponse extends AbstractResponse {
    
    @Nullable
    private String version;

}
