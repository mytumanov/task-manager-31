package ru.mtumanov.tm.client;

import org.jetbrains.annotations.NotNull;

import lombok.SneakyThrows;
import ru.mtumanov.tm.api.endpoint.ISystemEndpoint;
import ru.mtumanov.tm.dto.request.ServerAboutRequest;
import ru.mtumanov.tm.dto.request.ServerVersionRequest;
import ru.mtumanov.tm.dto.response.ServerAboutResponse;
import ru.mtumanov.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @Override
    @NotNull
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest serverAboutRequest) {
        return (ServerAboutResponse) call(serverAboutRequest);
    }

    @Override
    @NotNull
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest serverVersionRequest) {
        return (ServerVersionResponse) call(serverVersionRequest);
    }

    public static void main(String[] args) {
        try {
            @NotNull final SystemEndpointClient client = new SystemEndpointClient();
            client.connect();

            @NotNull final ServerAboutResponse about = client.getAbout(new ServerAboutRequest());
            System.out.println("about: \n" + about.getName() + "   " + about.getEmail());

            @NotNull final ServerVersionResponse version = client.getVersion(new ServerVersionRequest());
            System.out.println("version: \n" + version.getVersion());
            
            client.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
