package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Update project by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().updateByIndex(userId, index, name, description);
    }

}
