package ru.mtumanov.tm.task;

import java.net.Socket;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.component.Server;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    protected AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }
    
}
