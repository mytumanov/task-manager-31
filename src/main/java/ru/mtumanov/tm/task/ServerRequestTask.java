package ru.mtumanov.tm.task;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import javax.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import ru.mtumanov.tm.component.Server;
import ru.mtumanov.tm.dto.request.AbstractRequest;

public class ServerRequestTask extends AbstractServerSocketTask {

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    @Override
    public void run() {
        try {
            @NotNull final InputStream inputStream = socket.getInputStream();
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            @NotNull final AbstractRequest request = (AbstractRequest) objectInputStream.readObject();
            @Nullable Object response = server.call(request);
            @NotNull final OutputStream outputStream = socket.getOutputStream();
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(response);
            server.submit(new ServerRequestTask(server, socket));
        } catch (@NotNull final EOFException e) {
            server.getLoggerService().debug(e.getMessage());
        } catch (@NotNull final IOException | ClassNotFoundException e) {
            server.getLoggerService().error(e);
        }
    }
    
}
